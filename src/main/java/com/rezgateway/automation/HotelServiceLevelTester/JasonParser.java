package com.rezgateway.automation.HotelServiceLevelTester;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rezgateway.automation.pojo.AddToCartResponse;
import com.rezgateway.automation.pojo.AvailabilityResponse;

public class JasonParser {
	private static Logger logger;
	public JasonParser() {
		logger = Logger.getLogger(JasonParser.class);
	}

	public AvailabilityResponse convertAvailabilityResponseJsonToObject(String json) {
		AvailabilityResponse response = null;
		try {
    
			ObjectMapper mapper = new ObjectMapper();
            // convert JSON string to object
            logger.info("==> Converting json to object");
			response = mapper.readValue(json, AvailabilityResponse.class);
			logger.info("==> Json parsed " + response.toString());
			// response.printAll();

		} catch (JsonGenerationException e) {
			logger.fatal("==>Json generation Error while pharsing the json ", e);
			e.printStackTrace();
		} catch (JsonMappingException e) {
			logger.fatal("==> Json mapping Error while pharsing the json ", e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.fatal("==> General Error while pharsing the json ", e);
			e.printStackTrace();
		}

		logger.info("==> Returning pharsed object ");
		return response;
	}
	
	public AddToCartResponse convertAddToCartResponseJsonToObject(String json) {
		AddToCartResponse response = null;
		try {
    
			ObjectMapper mapper = new ObjectMapper();
            // convert JSON string to object
            logger.info("==> Converting json to object");
			response = mapper.readValue(json, AddToCartResponse.class);
			logger.info("==> Json parsed " + response.toString());
			// response.printAll();

		} catch (JsonGenerationException e) {
			logger.fatal("==>Json generation Error while pharsing the json ", e);
			e.printStackTrace();
		} catch (JsonMappingException e) {
			logger.fatal("==> Json mapping Error while pharsing the json ", e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.fatal("==> General Error while pharsing the json ", e);
			e.printStackTrace();
		}

		logger.info("==> Returning pharsed object ");
		return response;
	}

}
