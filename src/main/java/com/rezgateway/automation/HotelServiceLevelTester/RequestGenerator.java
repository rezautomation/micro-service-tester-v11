package com.rezgateway.automation.HotelServiceLevelTester;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.testng.Assert;

import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.Data;
import com.rezgateway.automation.pojo.HotelSearchDetails;

public class RequestGenerator extends RunnerBase {

	private static Logger logger;
	public RequestGenerator() {

		
		logger = Logger.getLogger(RequestGenerator.class);
 	}
	
	HashMap<String, String> prop       = null;
	HashMap<String, String> properties = null;
	HashMap<String, String> addtocartproperties = null;
	
	


	public String sendAvailabilityRequest(HotelSearchDetails details) {
		
        logger.info("==> Reading Properties");
        
        jenkinsParameterprocessor jpp= new jenkinsParameterprocessor();
        
		try {
			
			prop       = readProperty("Resources/config.properties");
			properties = readProperty("Resources/parameters.properties");
			logger.info("properties loaded successfully ==> Config -->"+prop.toString()+" searchprop -->"+properties.toString());
			
		} catch (Exception e1) {
			logger.fatal("Failed to read properties",e1);
		}
		
		
		

		logger.info("====== Replacing Properties======");
		
		
		
		properties.replace("portal", jpp.portalnamefinder());
		
		String transection = jpp.portalnamefinder().toLowerCase().concat("-"+details.getTransectionID());
		logger.info("Transection--->" + transection);
		properties.replace("transectionId",transection );
		
		details.setTransectionID(transection);
		
		properties.replace("cityCode", jpp.Searchcitycode());
		properties.replace("checkInDate", details.getCheckInDate());
		properties.replace("checkOutDate", details.getCheckOutDate());
		properties.replace("numberOfNights", details.getNumberOfNights());
		
	
		
		
		
		
		logger.info("Replacing city code======>"+jpp.Searchcitycode());

		Iterator<Map.Entry<String, String>> itr = properties.entrySet().iterator();
		List<NameValuePair> arguments = new ArrayList<>();
		
		while (itr.hasNext()) {
			Map.Entry<String,String> entry = (Map.Entry<String,String>) itr.next();
            arguments.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		}
		
		
		String       jsondata = "";
		try {
			
			
			
			
			
			logger.info("===> Sending post to url : " + jpp.Urlprocessor().concat("-hotel-availability-service/availability/hotels") );
			HttpClient client     = HttpClientBuilder.create().build();
			HttpPost post         = new HttpPost(jpp.Urlprocessor().concat("-hotel-availability-service/availability/hotels"));
			
			post.setEntity(new UrlEncodedFormEntity(arguments));
			HttpResponse response = client.execute(post);
			
			
			
			jsondata              = EntityUtils.toString(response.getEntity());
			
			
			logger.info("Response recieved -->" + jsondata ); 
			
		} catch (IOException e) {
			logger.fatal("Post request failed",e);
		}

		return jsondata;
	}

	
	
	public String sendAddtoCartRequest(HotelSearchDetails details, AvailabilityResponse resp){
		
		
		
		logger.info("==> Reading Properties");
        
        jenkinsParameterprocessor jpp= new jenkinsParameterprocessor();
        
        
        
		try {
			
			addtocartproperties = readProperty("Resources/addtocart.properties");
			
			logger.info("addtocartproperties loaded successfully ==> Config -->"+addtocartproperties.toString());
			
		} catch (Exception e1) {
			logger.fatal("Failed to read properties",e1);
		}
		
		
		
		
		
		
		String       jsondata = "";
		
	try {
			
			
			
			
			
			logger.info("===> Sending post to url : " + jpp.Urlprocessor().concat("-hotel-cart-service/cart/add") );
			HttpClient client     = HttpClientBuilder.create().build();
			
			
			
			String Key =URLEncoder.encode(resp.getResp().getKey(),"UTF-8");
			
			String param = "?transectionId="+details.getTransectionID()+"&selected="+SelectedHotelStringGenerator(details.isIsinternal(),resp)+"&product="+ addtocartproperties.get("product")+"&key="+Key+"&bookType="+addtocartproperties.get("bookType")+"&from="+ addtocartproperties.get("from")+"&payment="+addtocartproperties.get("payment")+"&vacation="+ addtocartproperties.get("vacation")+"&transfer="+addtocartproperties.get("transfer");
            String url   = jpp.Urlprocessor().concat("-hotel-cart-service/cart/add").concat(param);
            logger.info("url------>"+url);
			
            
            HttpPost post         = new HttpPost(url);
			
			
			
			/*HttpParams params = new BasicHttpParams();
			
			
			
			
			params.setParameter("transectionId", details.getTransectionID());
			params.setParameter("selected", "51236%253CSP%253EDBT_RO_GC-ALL1_DX_0%253CSP%253ERO%253CSP%253E53568%253CRW%253E");
			params.setParameter("product", properties.get("product"));
			params.setParameter("key", resp.getResp().getKey());
			params.setParameter("bookType", properties.get("bookType"));
			params.setParameter("from", properties.get("from"));
			params.setParameter("payment", 	properties.get("payment"));
			params.setParameter("vacation", properties.get("vacation"));
			params.setParameter("transfer", properties.get("transfer"));
			
			
			
			
			post.setParams(params);
			*/
			
			HttpResponse response = client.execute(post);
			
			
			
			jsondata              = EntityUtils.toString(response.getEntity());
			
			
			logger.info("Response recieved -->" + jsondata ); 
			
		} catch (IOException e) {
			logger.fatal("Post request failed",e);
		}
		
		
		return jsondata;
	}

	
	
	public String SelectedHotelStringGenerator(Boolean isIsinternal, AvailabilityResponse resp) throws UnsupportedEncodingException{
		
		
		
		String selectedvalue = null;
		
		String hotelcode = "";
		String roomcode ="";
		String ratecode ="";
		String ridecode ="";
		
		if(isIsinternal){
			
			
			
				if (resp.getStatus().equalsIgnoreCase("200")) {
					int hotelcount = resp.getResp().getHotellist().size();

					if (hotelcount > 0) {
						
		               
		               
		               ArrayList<Data> hotellist = resp.getResp().getHotellist();
		       		Iterator<Data> itr = hotellist.iterator();
		       		boolean isHotelFound = false;
		       		while (itr.hasNext()) {
		       			
		       			Data hotelinfo = itr.next();
		       			
		       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city"))){
		       				isHotelFound = true;
		       				
		       				
		       				hotelcode = hotelinfo.getHotelCode();
		       				roomcode =  hotelinfo.getRoomlist().get(0).getRoomCode();
		       				ratecode =  hotelinfo.getRoomlist().get(0).getRateCode();
		       				ridecode =  hotelinfo.getHotelRideCode();
		       				
		       				
		       				System.out.println("kaushal   "+hotelcode+" "+roomcode+" "+ratecode+" "+ridecode);
		       			
		       			}
		       				
		       		}
		       		
		       		if(!isHotelFound){
		       				logger.info("internal Hotel is not Available"); 
		       		
		       		}
		       		
		               
		            } else {
		            	logger.info("Hotel Results(Internal and thirdparty) are not Available");
					   
			

					}
				}else {
					logger.info("Error Response");
					  
				}
			 
			 
			 
			 
					
			
			
		}
		
		else{
			
			if (resp.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = resp.getResp().getHotellist().size();

				if (hotelcount > 0) {
					
	               
	               
	               ArrayList<Data> hotellist = resp.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getInternalHotel().equalsIgnoreCase("false")){
	       				
	       				
	       				
	       				logger.info("dinethra");
	       				
	       				isHotelFound = true;
	       				
	       				
	       				hotelcode = hotelinfo.getHotelCode();
	       				roomcode =  hotelinfo.getRoomlist().get(0).getRoomCode();
	       				ratecode =  hotelinfo.getRoomlist().get(0).getRateCode();
	       				ridecode =  hotelinfo.getHotelRideCode();
	       				
	       				System.out.println("kaushal   "+hotelcode+" "+roomcode+" "+ratecode+" "+ridecode);
	       				
	       				
	       				break;
	       			
	       			}
	       				
	       		}
	       		
	       		if(!isHotelFound){
	       				logger.info("third party  Hotel is not Available"); 
	       		
	       		}
	       		
	               
	            } else {
	            	logger.info("Hotel Results(Internal and thirdparty) are not Available");
				   
		

				}
			}else {
				logger.info("Error Response");
				  
			}
		 
		
			
		}
		
		
		selectedvalue = hotelcode+"<SP>"+roomcode+"<SP>"+ratecode+"<SP>"+ridecode+"<RW>";
		
		
		String	newselectedvalue=URLEncoder.encode(selectedvalue, "UTF-8");
		
		return newselectedvalue;
				
		
	}
	
}
