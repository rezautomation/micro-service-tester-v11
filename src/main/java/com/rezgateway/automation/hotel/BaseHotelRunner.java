package com.rezgateway.automation.hotel;

import com.rezgateway.automation.HotelServiceLevelTester.JasonParser;
import com.rezgateway.automation.HotelServiceLevelTester.RequestGenerator;
import com.rezgateway.automation.pojo.AddToCartResponse;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.HotelSearchDetails;

public class BaseHotelRunner {
	
	
	
	public AvailabilityResponse doHotelSearch(HotelSearchDetails search){
		
		RequestGenerator generator = new RequestGenerator();
		String data = generator.sendAvailabilityRequest(search);
		
		JasonParser parser = new JasonParser();
		return parser.convertAvailabilityResponseJsonToObject(data);
		
	}
	
	
	
	public AddToCartResponse hotelAddToCart(HotelSearchDetails search,AvailabilityResponse response){
		
		
		RequestGenerator generator = new RequestGenerator();
		
		
		String data = generator.sendAddtoCartRequest(search,response);
		
		JasonParser parser = new JasonParser();
		
		return parser.convertAddToCartResponseJsonToObject(data);
		
	}
	
}
