package com.rezgateway.automation.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddToCartResponse {
	
	public String getStatus() {
		return status;
	}
	
	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	
	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}
	public String getItemcode() {
		return itemcode;
	}
	
	@JsonProperty("itemcode")
	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}
	public String getAdditionalValues() {
		return additionalValues;
	}
	
	@JsonProperty("additionalValues")
	public void setAdditionalValues(String additionalValues) {
		this.additionalValues = additionalValues;
	}
	
	
	
	
	
	String status;
	String message;
	String itemcode;
	String additionalValues;

}
