package com.rezgateway.automation.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AmenityList {

	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public String getAmnId() {
		return amnId;
	}

	@JsonProperty("amnId")
	public void setAmnId(String amnId) {
		this.amnId = amnId;
	}

	public String getDescription() {
		return description;
	}

	@JsonProperty("descrip")
	public void setDescription(String description) {
		this.description = description;
	}

	public String getFilterable() {
		return filterable;
	}

	@JsonProperty("filterable")
	public void setFilterable(String filterable) {
		this.filterable = filterable;
	}

	public String getImgName() {
		return imgName;
	}

	@JsonProperty("imgName")
	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	String id = "";
	String name = "";
	String amnId = "";
	String description = "";
	String filterable = "";
	String imgName = "";

}
