package com.rezgateway.automation.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AvailabilityResponse {

	String ServiceName = "";
	String Profiles = "";
	String Label = "";
	String Version = "";
	String Status = "";
	Response resp = null;

	public String getServiceName() {
		return ServiceName;
	}

	@JsonProperty("service_name")
	public void setServiceName(String serviceName) {
		ServiceName = serviceName;
	}

	public String getProfiles() {
		return Profiles;
	}

	@JsonProperty("profiles")
	public void setProfiles(String profiles) {
		Profiles = profiles;
	}

	public String getLabel() {
		return Label;
	}

	@JsonProperty("label")
	public void setLabel(String label) {
		Label = label;
	}

	public String getVersion() {
		return Version;
	}

	@JsonProperty("version")
	public void setVersion(String version) {
		Version = version;
	}

	public String getStatus() {
		return Status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		Status = status;
	}

	public Response getResp() {
		return resp;
	}

	@JsonProperty("response")
	public void setResp(Response resp) {
		this.resp = resp;
	}
	
	


}
