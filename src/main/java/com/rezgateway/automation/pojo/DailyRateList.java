package com.rezgateway.automation.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyRateList {

	

	public String getId() {
		return id;
	}
	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	public String getTotalRate() {
		return totalRate;
	}
	@JsonProperty("totalRate")
	public void setTotalRate(String totalRate) {
		this.totalRate = totalRate;
	}

	public String getTotalRateDecimal() {
		return totalRateDecimal;
	}
	@JsonProperty("totalRateDecimal")
	public void setTotalRateDecimal(String totalRateDecimal) {
		this.totalRateDecimal = totalRateDecimal;
	}

	public String getNoOfDayInWeek() {
		return noOfDayInWeek;
	}
	@JsonProperty("noOfDayInWeek")
	public void setNoOfDayInWeek(String noOfDayInWeek) {
		this.noOfDayInWeek = noOfDayInWeek;
	}

	public String getDayInLongValue() {
		return dayInLongValue;
	}
	@JsonProperty("dayInLongValue")
	public void setDayInLongValue(String dayInLongValue) {
		this.dayInLongValue = dayInLongValue;
	}
	
	String id = "";
	String totalRate = "";
	String totalRateDecimal = "";
	String noOfDayInWeek = "";
	String dayInLongValue = "";

}
