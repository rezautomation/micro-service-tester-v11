package com.rezgateway.automation.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	@JsonProperty("contactNumber")
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getHotelName() {
		return hotelName;
	}

	@JsonProperty("hotelName")
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelSuburbId() {
		return hotelSuburbId;
	}

	@JsonProperty("hotelSuburbId")
	public void setHotelSuburbId(String hotelSuburbId) {
		this.hotelSuburbId = hotelSuburbId;
	}

	public String getHotelRideCode() {
		return hotelRideCode;
	}

	@JsonProperty("hotelRideCode")
	public void setHotelRideCode(String hotelRideCode) {
		this.hotelRideCode = hotelRideCode;
	}

	public String getHotelSuburbName() {
		return hotelSuburbName;
	}

	@JsonProperty("hotelSuburbName")
	public void setHotelSuburbName(String hotelSuburbName) {
		this.hotelSuburbName = hotelSuburbName;
	}

	public String getVacationPkg() {
		return vacationPkg;
	}

	@JsonProperty("vacationPkg")
	public void setVacationPkg(String vacationPkg) {
		this.vacationPkg = vacationPkg;
	}

	public String getHotelCode() {
		return hotelCode;
	}

	@JsonProperty("hotelCode")
	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}

	public String getHotelDescription() {
		return hotelDescription;
	}

	@JsonProperty("hotelDescription")
	public void setHotelDescription(String hotelDescription) {
		this.hotelDescription = hotelDescription;
	}

	public String getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	public String getThumbNailURL() {
		return thumbNailURL;
	}

	@JsonProperty("thumbNailURL")
	public void setThumbNailURL(String thumbNailURL) {
		this.thumbNailURL = thumbNailURL;
	}

	public String getPrefCurrency() {
		return prefCurrency;
	}

	@JsonProperty("prefCurrency")
	public void setPrefCurrency(String prefCurrency) {
		this.prefCurrency = prefCurrency;
	}

	public String getPrefCurrencyVal() {
		return prefCurrencyVal;
	}

	@JsonProperty("prefCurrencyVal")
	public void setPrefCurrencyVal(String prefCurrencyVal) {
		this.prefCurrencyVal = prefCurrencyVal;
	}

	public String getPrefCurrencyValDecimal() {
		return prefCurrencyValDecimal;
	}

	@JsonProperty("prefCurrencyValDecimal")
	public void setPrefCurrencyValDecimal(String prefCurrencyValDecimal) {
		this.prefCurrencyValDecimal = prefCurrencyValDecimal;
	}

	public String getVacPrefCurrencyVal() {
		return vacPrefCurrencyVal;
	}

	@JsonProperty("vacPrefCurrencyVal")
	public void setVacPrefCurrencyVal(String vacPrefCurrencyVal) {
		this.vacPrefCurrencyVal = vacPrefCurrencyVal;
	}

	public String getStartCategory() {
		return startCategory;
	}

	@JsonProperty("startCategory")
	public void setStartCategory(String startCategory) {
		this.startCategory = startCategory;
	}

	public String getStartCategoryValue() {
		return startCategoryValue;
	}

	@JsonProperty("startCategoryValue")
	public void setStartCategoryValue(String startCategoryValue) {
		this.startCategoryValue = startCategoryValue;
	}

	public String getInternalHotel() {
		return internalHotel;
	}

	@JsonProperty("internalHotel")
	public void setInternalHotel(String internalHotel) {
		this.internalHotel = internalHotel;
	}

	public String getOverallRating() {
		return overallRating;
	}

	@JsonProperty("overallRating")
	public void setOverallRating(String overallRating) {
		this.overallRating = overallRating;
	}

	public String getNoOfReviews() {
		return noOfReviews;
	}

	@JsonProperty("noOfReviews")
	public void setNoOfReviews(String noOfReviews) {
		this.noOfReviews = noOfReviews;
	}

	public String getRateContract() {
		return rateContract;
	}

	@JsonProperty("rateContract")
	public void setRateContract(String rateContract) {
		this.rateContract = rateContract;
	}

	public String getCommiContractType() {
		return commiContractType;
	}

	@JsonProperty("commiContractType")
	public void setCommiContractType(String commiContractType) {
		this.commiContractType = commiContractType;
	}

	public String getHotelType() {
		return hotelType;
	}

	@JsonProperty("hotelType")
	public void setHotelType(String hotelType) {
		this.hotelType = hotelType;
	}

	public String getRatingsDisplayType() {
		return ratingsDisplayType;
	}

	@JsonProperty("ratingsDisplayType")
	public void setRatingsDisplayType(String ratingsDisplayType) {
		this.ratingsDisplayType = ratingsDisplayType;
	}

	public String getTotalRatingCount() {
		return totalRatingCount;
	}

	@JsonProperty("totalRatingCount")
	public void setTotalRatingCount(String totalRatingCount) {
		this.totalRatingCount = totalRatingCount;
	}

	public String getDistance() {
		return distance;
	}

	@JsonProperty("distance")
	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	@JsonProperty("fromLocation")
	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getPmsHotel() {
		return pmsHotel;
	}

	@JsonProperty("pmsHotel")
	public void setPmsHotel(String pmsHotel) {
		this.pmsHotel = pmsHotel;
	}

	public String getRideHotelId() {
		return rideHotelId;
	}

	@JsonProperty("rideHotelId")
	public void setRideHotelId(String rideHotelId) {
		this.rideHotelId = rideHotelId;
	}

	public String getCurrency() {
		return currency;
	}

	@JsonProperty("currency")
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getBookType() {
		return bookType;
	}

	@JsonProperty("bookType")
	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public String getAvg() {
		return avg;
	}

	@JsonProperty("avg")
	public void setAvg(String avg) {
		this.avg = avg;
	}

	public String getAvgDecimal() {
		return avgDecimal;
	}

	@JsonProperty("avgDecimal")
	public void setAvgDecimal(String avgDecimal) {
		this.avgDecimal = avgDecimal;
	}

	public String getAvgvac() {
		return avgvac;
	}

	@JsonProperty("avgvac")
	public void setAvgvac(String avgvac) {
		this.avgvac = avgvac;
	}

	public String getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}

	public String getVendorId() {
		return vendorId;
	}

	@JsonProperty("vendorId")
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	@JsonProperty("vendorName")
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getHotelOnlyVendor() {
		return hotelOnlyVendor;
	}

	@JsonProperty("hotelOnlyVendor")
	public void setHotelOnlyVendor(String hotelOnlyVendor) {
		this.hotelOnlyVendor = hotelOnlyVendor;
	}

	public String getHotelSearchId() {
		return hotelSearchId;
	}

	@JsonProperty("hotelSearchId")
	public void setHotelSearchId(String hotelSearchId) {
		this.hotelSearchId = hotelSearchId;
	}

	public String getCountry() {
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	public String getLatitude() {
		return latitude;
	}

	@JsonProperty("latitude")
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	@JsonProperty("longitude")
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getStrikeOutValue() {
		return strikeOutValue;
	}

	@JsonProperty("strikeOutValue")
	public void setStrikeOutValue(String strikeOutValue) {
		this.strikeOutValue = strikeOutValue;
	}

	public String getStrikeOutValueDecimal() {
		return strikeOutValueDecimal;
	}

	@JsonProperty("strikeOutValueDecimal")
	public void setStrikeOutValueDecimal(String strikeOutValueDecimal) {
		this.strikeOutValueDecimal = strikeOutValueDecimal;
	}

	public String getMultiple() {
		return multiple;
	}

	@JsonProperty("multiple")
	public void setMultiple(String multiple) {
		this.multiple = multiple;
	}

	public String getMultiplelist() {
		return multiplelist;
	}

	@JsonProperty("multiplelist")
	public void setMultiplelist(String multiplelist) {
		this.multiplelist = multiplelist;
	}

	public String getPromoInclude() {
		return promoInclude;
	}

	@JsonProperty("promoInclude")
	public void setPromoInclude(String promoInclude) {
		this.promoInclude = promoInclude;
	}

	public String getFeatured() {
		return featured;
	}

	@JsonProperty("featured")
	public void setFeatured(String featured) {
		this.featured = featured;
	}

	public String getBestrate() {
		return bestrate;
	}

	@JsonProperty("bestrate")
	public void setBestrate(String bestrate) {
		this.bestrate = bestrate;
	}

	public String getDealOfTheDayHotel() {
		return dealOfTheDayHotel;
	}

	@JsonProperty("dealOfTheDayHotel")
	public void setDealOfTheDayHotel(String dealOfTheDayHotel) {
		this.dealOfTheDayHotel = dealOfTheDayHotel;
	}

	public String getRideTracer() {
		return rideTracer;
	}

	@JsonProperty("rideTracer")
	public void setRideTracer(String rideTracer) {
		this.rideTracer = rideTracer;
	}

	public ArrayList<RoomsList> getRoomlist() {
		return roomlist;
	}

	@JsonProperty("roomsList")
	public void setRoomlist(ArrayList<RoomsList> roomlist) {
		this.roomlist = roomlist;
	}

	public int[] getTravelTypeIdList() {
		return travelTypeIdList;
	}

	@JsonProperty("travelTypeIdList")
	public void setTravelTypeIdList(int[] travelTypeIdList) {
		this.travelTypeIdList = travelTypeIdList;
	}

	public String getCheckinDate() {
		return checkinDate;
	}

	@JsonProperty("checkinDate")
	public void setCheckinDate(String checkinDate) {
		this.checkinDate = checkinDate;
	}

	public String getCheckoutDate() {
		return checkoutDate;
	}

	@JsonProperty("checkoutDate")
	public void setCheckoutDate(String checkoutDate) {
		this.checkoutDate = checkoutDate;
	}

	public String getGuestlist() {
		return guestlist;
	}

	@JsonProperty("guestlist")
	public void setGuestlist(String guestlist) {
		this.guestlist = guestlist;
	}

	public String getClientCountry() {
		return clientCountry;
	}

	@JsonProperty("clientCountry")
	public void setClientCountry(String clientCountry) {
		this.clientCountry = clientCountry;
	}

	public String getTransferIncluded() {
		return transferIncluded;
	}

	@JsonProperty("transferIncluded")
	public void setTransferIncluded(String transferIncluded) {
		this.transferIncluded = transferIncluded;
	}

	public String getRateBasedOn() {
		return rateBasedOn;
	}

	@JsonProperty("rateBasedOn")
	public void setRateBasedOn(String rateBasedOn) {
		this.rateBasedOn = rateBasedOn;
	}

	public String getIncusiveTransfer() {
		return incusiveTransfer;
	}

	@JsonProperty("incusiveTransfer")
	public void setIncusiveTransfer(String incusiveTransfer) {
		this.incusiveTransfer = incusiveTransfer;
	}

	public String getSupplimentariesAvailable() {
		return supplimentariesAvailable;
	}

	@JsonProperty("supplimentariesAvailable")
	public void setSupplimentariesAvailable(String supplimentariesAvailable) {
		this.supplimentariesAvailable = supplimentariesAvailable;
	}

	public ArrayList<AmenityList> getAmenitylist() {
		return amenitylist;
	}

	@JsonProperty("amenityList")
	public void setAmenitylist(ArrayList<AmenityList> amenitylist) {
		this.amenitylist = amenitylist;
	}

	public String getAvailableToBook() {
		return availableToBook;
	}

	@JsonProperty("availableToBook")
	public void setAvailableToBook(String availableToBook) {
		this.availableToBook = availableToBook;
	}

	String id = "";
	String contactNumber = "";
	String hotelName = "";
	String hotelSuburbId = "";
	String hotelRideCode = "";
	String hotelSuburbName = "";
	String vacationPkg = "";
	String hotelCode = "";
	String hotelDescription = "";
	String address = "";
	String thumbNailURL = "";
	String prefCurrency = "";
	String prefCurrencyVal = "";
	String prefCurrencyValDecimal = "";
	String vacPrefCurrencyVal = "";
	String startCategory = "";
	String startCategoryValue = "";
	String internalHotel = "";
	String overallRating = "";
	String noOfReviews = "";
	String rateContract = "";
	String commiContractType = "";
	String hotelType = "";
	String ratingsDisplayType = "";
	String totalRatingCount = "";
	String distance = "";
	String fromLocation = "";
	String pmsHotel = "";
	String rideHotelId = "";
	String currency = "";
	String bookType = "";
	String avg = "";
	String avgDecimal = "";
	String avgvac = "";
	String city = "";
	String vendorId = "";
	String vendorName = "";
	String hotelOnlyVendor = "";
	String hotelSearchId = "";
	String country = "";
	String latitude = "";
	String longitude = "";
	String strikeOutValue = "";
	String strikeOutValueDecimal = "";
	String multiple = "";
	String multiplelist = "";
	String promoInclude = "";
	String featured = "";
	String bestrate = "";
	String dealOfTheDayHotel = "";
	String rideTracer = "";

	ArrayList<RoomsList> roomlist = null;

	int[] travelTypeIdList = null;

	String checkinDate = "";
	String checkoutDate = "";
	String guestlist = "";
	String clientCountry = "";
	// String supplementaryList = "";
	String transferIncluded = "";
	String rateBasedOn = "";
	String incusiveTransfer = "";
	// String unavailableRoomsList = "";
	String supplimentariesAvailable = "";

	ArrayList<AmenityList> amenitylist = null;

	String availableToBook = "";

}
