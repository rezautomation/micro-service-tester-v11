package com.rezgateway.automation.pojo;


//contains only mandatory data for the search

public class HotelSearchDetails {

	

	public String getBookingChannel() {
		return bookingChannel;
	}

	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}

	public String getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getGuestList() {
		return guestList;
	}

	public void setGuestList(String guestList) {
		this.guestList = guestList;
	}

	public String getNumberOfAdults() {
		return numberOfAdults;
	}

	public void setNumberOfAdults(String numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}

	public String getNumberOfChilds() {
		return numberOfChilds;
	}

	public void setNumberOfChilds(String numberOfChilds) {
		this.numberOfChilds = numberOfChilds;
	}

	public String getNumberOfNights() {
		return numberOfNights;
	}

	public void setNumberOfNights(String numberOfNights) {
		this.numberOfNights = numberOfNights;
	}

	public String getNumberOfRooms() {
		return numberOfRooms;
	}

	public void setNumberOfRooms(String numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	public String getTransectionID() {
		return transectionID;
	}

	public void setTransectionID(String transectionID) {
		this.transectionID = transectionID;
	}

	String bookingChannel = "";
	String checkInDate = "";
	String checkOutDate = "";
	String cityCode = "";
	String cityName = "";
	String countryCode = "";
	String countryName = "";
	String guestList = "";
	String numberOfAdults = "";
	String numberOfChilds = "";
	String numberOfNights = "";
	String numberOfRooms = "";
	String transectionID = "";


	boolean isinternal;


	public boolean isIsinternal() {
		return isinternal;
	}

	public void setIsinternal(boolean isinternal) {
		this.isinternal = isinternal;
	}
	
}
