package com.rezgateway.automation.pojo;

import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

	String status = "";

	String alternatives = "";

	String alternativeAvailable = "";

	String message = "";

	String key = "";

	String tracerId = "";

	ArrayList<Data> hotellist = null;

	public ArrayList<Data> getHotellist() {
		return hotellist;
	}

	@JsonProperty("data")
	public void setHotellist(ArrayList<Data> hotellist) {
		this.hotellist = hotellist;
	}

	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	public String getAlternatives() {
		return alternatives;
	}

	@JsonProperty("alternatives")
	public void setAlternatives(String alternatives) {
		this.alternatives = alternatives;
	}

	public String getAlternativeAvailable() {
		return alternativeAvailable;
	}

	@JsonProperty("alternativeAvailable")
	public void setAlternativeAvailable(String alternativeAvailable) {
		this.alternativeAvailable = alternativeAvailable;
	}

	public String getMessage() {
		return message;
	}

	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

	public String getKey() {
		return key;
	}

	@JsonProperty("key")
	public void setKey(String key) {
		this.key = key;
	}

	public String getTracerId() {
		return tracerId;
	}

	@JsonProperty("tracerId")
	public void setTracerId(String tracerId) {
		this.tracerId = tracerId;
	}



}
