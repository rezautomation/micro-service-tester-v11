package com.rezgateway.automation.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomsList {

	

	public String getId() {
		return id;
	}
	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	public String getRoomId() {
		return roomId;
	}
	@JsonProperty("roomId")
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	
	public String getRoomType() {
		return roomType;
	}
	@JsonProperty("roomType")
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getRoomCode() {
		return roomCode;
	}
	@JsonProperty("roomCode")
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getStrikeOutValue() {
		return strikeOutValue;
	}
	@JsonProperty("strikeOutValue")
	public void setStrikeOutValue(String strikeOutValue) {
		this.strikeOutValue = strikeOutValue;
	}

	public String getRateCode() {
		return rateCode;
	}
	@JsonProperty("rateCode")
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getRoomAdultCount() {
		return roomAdultCount;
	}
	@JsonProperty("roomAdultCount")
	public void setRoomAdultCount(String roomAdultCount) {
		this.roomAdultCount = roomAdultCount;
	}

	public String getRoomChildCount() {
		return roomChildCount;
	}
	@JsonProperty("roomChildCount")
	public void setRoomChildCount(String roomChildCount) {
		this.roomChildCount = roomChildCount;
	}

	public String getOccupancyId() {
		return occupancyId;
	}
	@JsonProperty("occupancyId")
	public void setOccupancyId(String occupancyId) {
		this.occupancyId = occupancyId;
	}

	public String getPromoAvaiable() {
		return promoAvaiable;
	}
	@JsonProperty("promoAvaiable")
	public void setPromoAvaiable(String promoAvaiable) {
		this.promoAvaiable = promoAvaiable;
	}

	public String getPromoTitle() {
		return promoTitle;
	}
	@JsonProperty("promoTitle")
	public void setPromoTitle(String promoTitle) {
		this.promoTitle = promoTitle;
	}

	public String getPromoCode() {
		return promoCode;
	}
	@JsonProperty("promoCode")
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getFullPromoPeriod() {
		return fullPromoPeriod;
	}
	@JsonProperty("fullPromoPeriod")
	public void setFullPromoPeriod(String fullPromoPeriod) {
		this.fullPromoPeriod = fullPromoPeriod;
	}

	public String getRate() {
		return rate;
	}
	@JsonProperty("rate")
	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRateDecimal() {
		return rateDecimal;
	}
	@JsonProperty("rateDecimal")
	public void setRateDecimal(String rateDecimal) {
		this.rateDecimal = rateDecimal;
	}

	public String getRoomName() {
		return roomName;
	}
	@JsonProperty("roomName")
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getBedType() {
		return bedType;
	}
	@JsonProperty("bedType")
	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public String getRateType() {
		return rateType;
	}
	@JsonProperty("rateType")
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getPrefCurrency() {
		return prefCurrency;
	}
	@JsonProperty("prefCurrency")
	public void setPrefCurrency(String prefCurrency) {
		this.prefCurrency = prefCurrency;
	}

	public String getTransferRate() {
		return transferRate;
	}
	@JsonProperty("transferRate")
	public void setTransferRate(String transferRate) {
		this.transferRate = transferRate;
	}

	public String getPrefCurrencyValue() {
		return prefCurrencyValue;
	}
	@JsonProperty("prefCurrencyValue")
	public void setPrefCurrencyValue(String prefCurrencyValue) {
		this.prefCurrencyValue = prefCurrencyValue;
	}

	public String getViewTypeId() {
		return viewTypeId;
	}
	@JsonProperty("viewTypeId")
	public void setViewTypeId(String viewTypeId) {
		this.viewTypeId = viewTypeId;
	}

	public String getMealPlanCode() {
		return mealPlanCode;
	}
	@JsonProperty("mealPlanCode")
	public void setMealPlanCode(String mealPlanCode) {
		this.mealPlanCode = mealPlanCode;
	}

	public String getDescription() {
		return description;
	}
	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public String getRateContractId() {
		return rateContractId;
	}
	@JsonProperty("rateContractId")
	public void setRateContractId(String rateContractId) {
		this.rateContractId = rateContractId;
	}

	public String getContractInventoryTypeId() {
		return contractInventoryTypeId;
	}
	@JsonProperty("contractInventoryTypeId")
	public void setContractInventoryTypeId(String contractInventoryTypeId) {
		this.contractInventoryTypeId = contractInventoryTypeId;
	}

	public ArrayList<DailyRateList> getDailyRateList() {
		return dailyRateList;
	}
	@JsonProperty("dailyRateList")
	public void setDailyRateList(ArrayList<DailyRateList> dailyRateList) {
		this.dailyRateList = dailyRateList;
	}

	public ArrayList<AmenityList> getAmenitylist() {
		return amenitylist;
	}
	@JsonProperty("amenities")
	public void setAmenitylist(ArrayList<AmenityList> amenitylist) {
		this.amenitylist = amenitylist;
	}

	public ArrayList<Promotions> getPromolist() {
		return Promolist;
	}
	@JsonProperty("promotions")
	public void setPromolist(ArrayList<Promotions> promolist) {
		Promolist = promolist;
	}

	public String getBookType() {
		return bookType;
	}
	@JsonProperty("bookType")
	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public ArrayList<Supplementaries> getSupplementarylist() {
		return supplementarylist;
	}
	@JsonProperty("supplementaries")
	public void setSupplementarylist(
			ArrayList<Supplementaries> supplementarylist) {
		this.supplementarylist = supplementarylist;
	}

	public String getUnavailableReason() {
		return unavailableReason;
	}
	@JsonProperty("unavailableReason")
	public void setUnavailableReason(String unavailableReason) {
		this.unavailableReason = unavailableReason;
	}

	public String getAvailableToBook() {
		return availableToBook;
	}
	@JsonProperty("availableToBook")
	public void setAvailableToBook(String availableToBook) {
		this.availableToBook = availableToBook;
	}

	String id = "";
	String roomId = "";
	String roomType = "";
	String roomCode = "";
	String strikeOutValue = "";
	String rateCode = "";
	String roomAdultCount = "";
	String roomChildCount = "";

	String occupancyId = "";
	String promoAvaiable = "";

	String promoTitle = "";
	String promoCode = "";
	String fullPromoPeriod = "";
	String rate = "";

	String rateDecimal = "";
	String roomName = "";
	String bedType = "";
	String rateType = "";
	String prefCurrency = "";
	String transferRate = "";
	String prefCurrencyValue = "";
	String viewTypeId = "";
	String mealPlanCode = "";
	String description = "";
	String rateContractId = "";
	String contractInventoryTypeId = "";

	ArrayList<DailyRateList> dailyRateList = null;

	ArrayList<AmenityList> amenitylist = null;
	ArrayList<Promotions> Promolist = null;

	String bookType = "";

	ArrayList<Supplementaries> supplementarylist = null;

	String unavailableReason = "";
	String availableToBook = "";

}
