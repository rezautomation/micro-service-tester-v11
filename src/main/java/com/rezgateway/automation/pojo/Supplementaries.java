package com.rezgateway.automation.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Supplementaries {

	public String getSupplementaryId() {
		return supplementaryId;
	}
	@JsonProperty("supplementaryId")
	public void setSupplementaryId(String supplementaryId) {
		this.supplementaryId = supplementaryId;
	}

	public String getSupplementaryName() {
		return supplementaryName;
	}
	@JsonProperty("supplementaryName")
	public void setSupplementaryName(String supplementaryName) {
		this.supplementaryName = supplementaryName;
	}

	public String getApplicableDate() {
		return applicableDate;
	}
	@JsonProperty("applicableDate")
	public void setApplicableDate(String applicableDate) {
		this.applicableDate = applicableDate;
	}

	public String getChargedBaseedOn() {
		return chargedBaseedOn;
	}
	@JsonProperty("chargedBaseedOn")
	public void setChargedBaseedOn(String chargedBaseedOn) {
		this.chargedBaseedOn = chargedBaseedOn;
	}

	public String getAdultSellRate() {
		return adultSellRate;
	}
	@JsonProperty("adultSellRate")
	public void setAdultSellRate(String adultSellRate) {
		this.adultSellRate = adultSellRate;
	}

	public String getChildSellRate() {
		return childSellRate;
	}
	@JsonProperty("childSellRate")
	public void setChildSellRate(String childSellRate) {
		this.childSellRate = childSellRate;
	}

	public String getMandateryStatus() {
		return mandateryStatus;
	}
	@JsonProperty("mandateryStatus")
	public void setMandateryStatus(String mandateryStatus) {
		this.mandateryStatus = mandateryStatus;
	}

	public String getChangableStatus() {
		return changableStatus;
	}
	@JsonProperty("changableStatus")
	public void setChangableStatus(String changableStatus) {
		this.changableStatus = changableStatus;
	}

	public String getSupplementaryCurrency() {
		return supplementaryCurrency;
	}
	@JsonProperty("supplementaryCurrency")
	public void setSupplementaryCurrency(String supplementaryCurrency) {
		this.supplementaryCurrency = supplementaryCurrency;
	}

	public String getSupplementaryDesc() {
		return supplementaryDesc;
	}
	@JsonProperty("supplementaryDesc")
	public void setSupplementaryDesc(String supplementaryDesc) {
		this.supplementaryDesc = supplementaryDesc;
	}

	String supplementaryId = "";
	String supplementaryName = "";
	String applicableDate = "";
	String chargedBaseedOn = "";
	String adultSellRate = "";
	String childSellRate = "";
	String mandateryStatus = "";
	String changableStatus = "";
	String supplementaryCurrency = "";
	String supplementaryDesc = "";

}
