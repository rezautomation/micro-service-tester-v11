package com.rezgateway.automation.HotelServiceLevelTester;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.hotel.BaseHotelRunner;
import com.rezgateway.automation.pojo.AddToCartResponse;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.Data;
import com.rezgateway.automation.pojo.HotelSearchDetails;

public class AddToCartTest extends RunnerBase{
	private static Logger logger; 
	
	public AddToCartTest() {
		logger = Logger.getLogger(HoteSearchTest.class);
	}
	
	
	AvailabilityResponse response = null;
	AddToCartResponse addtocartresponse = null;
	
	HotelSearchDetails details = new HotelSearchDetails();
	HashMap<String, String> properties = null;
	HashMap<String, String> prop = null;
	BaseHotelRunner runner = new BaseHotelRunner();
	
	boolean isinternal ;
	
	@Test(priority = 1)
	public void internalhoteladdtocart() throws Exception {

		details.setIsinternal(true);
		

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Specific Internal hotel Adding To The Cart ");
		result.setAttribute("Expected", "Hotel should be added to the cart");

		/******************************************************************/

		details.setCheckInDate("02/16/2019");
		details.setCheckOutDate("02/19/2019");
		details.setNumberOfNights("3");
		details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
		
		
		logger.info("======> Started Availability Search For Add to cart Test");
		
		response = runner.doHotelSearch(details);
		
		
		logger.info("======> Started Hotel Adding to the Cart function");
		addtocartresponse = runner.hotelAddToCart(details, response);
		
	
	
		
		if(addtocartresponse.getStatus().equalsIgnoreCase("ok")){
			
			result.setAttribute("Actual", "Internal Hotel Added to the Cart");
			
		}
		
		else{
			
			result.setAttribute("Actual", "Internal Hotel not Added to the Cart "+ addtocartresponse.getMessage());
			Assert.fail();
		}
		
		
	}
	
	
	@Test(priority = 2)
	public void sameinternalhoteladdingcarttwice() throws Exception {

		details.setIsinternal(true);
		

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Specific Internal Hotel Adding To The Cart Twice ");
		result.setAttribute("Expected", "Hotel Should Not be Added To The Cart");

		/******************************************************************/

				
		logger.info("======> Started same Hotel Adding to the Cart Twice Function");
		addtocartresponse = runner.hotelAddToCart(details, response);
		
	
		
		if(addtocartresponse.getStatus().equalsIgnoreCase("ok")){
			
			result.setAttribute("Actual", "Same Internal Hotel Added To The Cart Twice ");
			Assert.fail();
			
		}
		
		else if (addtocartresponse.getMessage().equalsIgnoreCase("This Combination is Already Added.")) {
			
			
			result.setAttribute("Actual", "Same Internal Hotel Not Added To The Cart Twice ");
			
			
		}
			
			
		
		
		
	}
	
	
	
	
	
		
		@Test(priority = 3)
		public void ThirdpartyResultsAvailabilitycheck() {

			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName",
					"Check Third Party Results Availability For Add To Cart Test");
			result.setAttribute("Expected",
					"Third party Results should be Available Atleast by one supplier");

			/******************************************************************/
			
			details.setCheckInDate("02/21/2019");
			details.setCheckOutDate("02/22/2019");
			details.setNumberOfNights("1");
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			
			logger.info("======> Started Availability Search For Add to cart Test");
			
			response = runner.doHotelSearch(details);

			
			

			ArrayList<Data> hotellist = response.getResp().getHotellist();

			HashMap<String, Integer> supplierhotel = new HashMap<String, Integer>();

			Iterator<Data> itr = hotellist.iterator();

			while (itr.hasNext()) {

				Data hotelinfo = itr.next();

				if (hotelinfo.getRideTracer().length() > 0) {

					String vendor = hotelinfo.getVendorName();

					if (supplierhotel.containsKey(vendor)) {

						int current = supplierhotel.get(vendor);
						supplierhotel.put(vendor, (current + 1));

					} else {
						supplierhotel.put(vendor, 1);

					}
				}

			}

			if (!supplierhotel.isEmpty()) {
				result.setAttribute("Actual", "Third Party Results Available--> "
						+ supplierhotel.toString());

			} else {
				result.setAttribute("Actual", "Third Party Results Not Available");
				Assert.fail("Third Party Results Not Available");
			}

		}
		
	
		
		
		@Test(dependsOnMethods = { "ThirdpartyResultsAvailabilitycheck" },priority = 4)
		public void thirdpartyaddtocartcheck() 
		{
			details.setIsinternal(false);
			
			
			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName","Check Third Party Hotel Add To Cart Test");
			result.setAttribute("Expected","Third party Hotel Should be added  To The Cart");

			/******************************************************************/
			
			
			
			
			
			logger.info("======> Started Thirdparty Hotel Adding to the Cart Function");
			addtocartresponse = runner.hotelAddToCart(details, response);
			
			
			if(addtocartresponse.getStatus().equalsIgnoreCase("ok")){
				
				result.setAttribute("Actual", "Third party Hotel Added to the Cart");
				
			}
			
			else{
				
				result.setAttribute("Actual", "Third party Hotel is not Added to the Cart "+ addtocartresponse.getMessage());
				Assert.fail();
			}
			
		}
	
}
