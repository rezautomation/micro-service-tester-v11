
/**
 * @author ${Charitha}
 *
 * ${tags}
 */

package com.rezgateway.automation.HotelServiceLevelTester;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.AssertJUnit;
//import org.junit.runner.Runner;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.hotel.BaseHotelRunner;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.DailyRateList;
import com.rezgateway.automation.pojo.Data;
import com.rezgateway.automation.pojo.HotelSearchDetails;
import com.rezgateway.automation.pojo.RoomsList;

//import com.rezgateway.automation.reports.ExtentTestNGReportBuilder;

public class HoteSearchTest extends RunnerBase {
	
	
	private static Logger logger; 
	public HoteSearchTest() {
		logger = Logger.getLogger(HoteSearchTest.class);
	}

	AvailabilityResponse response = null;
	HotelSearchDetails details = new HotelSearchDetails();
	HashMap<String, String> properties = null;
	HashMap<String, String> prop = null;
	
	BaseHotelRunner runner = new BaseHotelRunner();

	@Test(priority = 1)
	public void checkHotelResultsAvailability() throws Exception {

		
		

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check results availability ");
		result.setAttribute("Expected", "Results should be available");

		/******************************************************************/

		details.setCheckInDate("03/16/2019");
		details.setCheckOutDate("03/18/2019");
		details.setNumberOfNights("2");
		details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
		
		
		logger.info("======> Started Availability Search For First Test");
		
		response = runner.doHotelSearch(details);

		if (response.getStatus().equalsIgnoreCase("200")) {
			int hotelcount = response.getResp().getHotellist().size();

			if (hotelcount > 0) {
               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
            } else {
               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
			   Assert.fail();

			}
		}else {
			  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
			  Assert.fail();
		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 3)
	public void checkHotelNameisAvailable() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availablity of the Hotel Name for all hotels");
		result.setAttribute("Expected", "All Hotel Names should be available");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();
		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();

			try {
				AssertJUnit.assertTrue(hotelinfo.getHotelName().length() > 0);
				result.setAttribute("Actual", "All hotel name are existed");
			} catch (Exception e) {

				result.setAttribute("Actual", "Name Not exist of Hotel ID: "
						+ hotelinfo.getId());

				throw new Exception(e);

			}

			// result.setAttribute("Actual", value);
		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 2)
	public void ThirdpartyResultsAvailabilitycheck() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check third party Results Availability");
		result.setAttribute("Expected",
				"Third party Results should be Available Atleast by one supplier");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		HashMap<String, Integer> supplierhotel = new HashMap<String, Integer>();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();

			if (hotelinfo.getRideTracer().length() > 0) {

				String vendor = hotelinfo.getVendorName();

				if (supplierhotel.containsKey(vendor)) {

					int current = supplierhotel.get(vendor);
					supplierhotel.put(vendor, (current + 1));

				} else {
					supplierhotel.put(vendor, 1);

				}
			}

		}

		if (!supplierhotel.isEmpty()) {
			result.setAttribute("Actual", "Third Party Results Available--> "
					+ supplierhotel.toString());

		} else {
			result.setAttribute("Actual", "Third Party Results Not Available");
			Assert.fail("Third Party Results Not Available");
		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 4)
	public void InternalHotelDescriptionCheck() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availability of the Internal Hotel Description");
		result.setAttribute("Expected", "myhoteldescription");

		/******************************************************************/
		properties = readProperty("Resources/parameters.properties");
		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();
		boolean flag = false;
		while (itr.hasNext()) {

			Data hotelinfo = itr.next();

			if (hotelinfo.getHotelName().equalsIgnoreCase(
					"Shangrila_" + properties.get("cityName"))) {
				flag = true;
				result.setAttribute("Actual", hotelinfo.getHotelDescription());
				System.out.println(hotelinfo.getHotelDescription());

				Assert.assertTrue(hotelinfo.getHotelDescription().contains("myhoteldescription"));

			}
		}

		if (!flag) {
			result.setAttribute("Actual", "Specific TEST hotel Not available");
			Assert.fail("Specific TEST hotel Not available");
		}

	}

	@Test(dependsOnMethods = { "ThirdpartyResultsAvailabilitycheck" }, priority = 5)
	public void thirdpartyhotelDescriptionAvailablity() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availability of the Thirdparty Hotels Descriptions");
		result.setAttribute("Expected",
				"All Thirdparty hotel Decriptions should be available");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();

			if (hotelinfo.getRideTracer().length() > 0
					|| hotelinfo.getRideTracer() == null) {

				try {
					AssertJUnit.assertTrue(hotelinfo.getHotelDescription()
							.length() > 0);
					result.setAttribute("Actual",
							"All thirdparty hotel Descriptions are available");

				} catch (Exception e) {

					result.setAttribute(
							"Actual",
							hotelinfo.getHotelName()
									+ ": Third party hotel Descriptions is NOT available");

					break;
				}
			}

		}
	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 6)
	public void InternalHotelStarRatingchecker() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Internal Hotel Star Rating");
		result.setAttribute("Expected", "5 star");

		/******************************************************************/
		properties = readProperty("Resources/parameters.properties");
		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		boolean flag = false;
		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_" + properties.get("cityName"))) {
				
				flag = true;

				if (hotelinfo.getStartCategoryValue().equalsIgnoreCase("-")
						|| hotelinfo.getStartCategoryValue() == null) {

					result.setAttribute("Actual", hotelinfo.getHotelName()
							+ " Internal hotel Star Rating not exist");
					Assert.fail();
					break;
				}

				else {

					Assert.assertEquals(hotelinfo.getStartCategoryValue(),"5 Star Rating");
					result.setAttribute("Actual", "5 Star");
					break;
				}

			}
			
			

		}
		
		if(flag!=true){
			
			result.setAttribute("Actual", "Specific internal hotel is not available");
			Assert.fail();
			
		}
		
		
		
	}

	@Test(dependsOnMethods = { "ThirdpartyResultsAvailabilitycheck" }, priority = 7)
	public void ThirdpartyHotelStarRatingchecker() {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availability Of Thirdparty hotel Star Rating");
		result.setAttribute("Expected",
				"All third Party hotel should Contain Star category value");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getRideTracer().length() > 0) {

				if (hotelinfo.getStartCategoryValue().length() > 0) {

					result.setAttribute("Actual",
							"All Third party Hotels Contains Star ratings");
				}

				else {
					result.setAttribute(
							"Actual",
							"Star Rating not Available in hotel: "
									+ hotelinfo.getHotelName());

					Assert.fail();
				}

			}
		}
	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 8)
	public void HotelAddressChecker() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Availability Of Hotel Address");
		result.setAttribute("Expected",
				"All hotels should Contain Hotel Address");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getAddress().length() > 0) {

				result.setAttribute("Actual", "All Hotels contains an Address");
			}

			else {
				result.setAttribute(
						"Actual",
						"Hotel Address UnAvailable in hotel: "
								+ hotelinfo.getHotelName());

				Assert.fail();

			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 9)
	public void HotelSellingCurrencyChecker() {

		prop = readProperty("Resources/config.properties");

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Selling Currency of ALL Hotels");
		result.setAttribute("Expected",
				"Selling currency should be :" + prop.get("SellingCurrency"));

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			try {
				Assert.assertEquals(hotelinfo.getCurrency(),
						prop.get("SellingCurrency"));
				result.setAttribute("Actual",
						"Selling currency is existing for all hotels");
			} catch (Exception e) {

				result.setAttribute("Actual", "Selling currency should be :"
						+ prop.get("SellingCurrency"));
				Assert.fail();

				break;
			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 10)
	public void HotelLevelBookType() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Hotel Level Book Type is available for all hotels ");
		result.setAttribute("Expected",
				"Book Type should Be available for All hotels(Con,Req)");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getBookType().length() > 0) {

				if (hotelinfo.getBookType().equalsIgnoreCase("Con")
						|| hotelinfo.getBookType().equalsIgnoreCase("Req")) {

					result.setAttribute("Actual",
							"Book Type Available for all hotels");

				}

				else {

					result.setAttribute("Actual",
							"Hotel Level Book Type Is not Available in hotel : "
									+ hotelinfo.getHotelName());
					Assert.fail();
				}

			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 11)
	public void CityNameChecker() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Hotel City field is Available ");
		result.setAttribute("Expected",
				"Searched City should Be available for All hotels");

		/******************************************************************/

		properties = readProperty("Resources/parameters.properties");
		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			try {
				Assert.assertEquals(hotelinfo.getCity(),
						properties.get("cityName"));
				result.setAttribute("Actual", "All hotels Contains City");
			} catch (Exception e) {
				result.setAttribute("Actual", "City is not Existed : "
						+ hotelinfo.getHotelName());
				Assert.fail();
				break;
			}

		}

	}

	@Test(dependsOnMethods = { "ThirdpartyResultsAvailabilitycheck" }, priority = 12)
	public void AvailabilityOfVendorID() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Verify VendorID field is Available ");
		result.setAttribute("Expected",
				"VendorID should Be available for All Thirdparty Hotels");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getInternalHotel().equalsIgnoreCase("false")) {

				try {
					Assert.assertTrue(hotelinfo.getVendorId().length() > 0);
					result.setAttribute("Actual",
							"All Thirdparty hotels Contains a Vendor Id");
				} catch (Exception e) {
					result.setAttribute(
							"Actual",
							"Vendor ID is not Existed of : "
									+ hotelinfo.getHotelName());
					Assert.fail();

					throw new Exception(e);
				}

			}

		}

	}

	@Test(dependsOnMethods = { "ThirdpartyResultsAvailabilitycheck" }, priority = 13)
	public void AvailabilityOfVendorName() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Verify VendorName field is Available ");
		result.setAttribute("Expected",
				"VendorName should Be available for All Thirdparty Hotels");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getInternalHotel().equalsIgnoreCase("false")) {

				try {
					Assert.assertTrue(hotelinfo.getVendorName().length() > 0);
					result.setAttribute("Actual",
							"All Thirdparty hotels Contains a VendorName");
				} catch (Exception e) {
					result.setAttribute(
							"Actual",
							"VendorName is not Existed of : "
									+ hotelinfo.getHotelName());
					Assert.fail();
					throw new Exception(e);
				}

			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 14)
	public void Availabilityoflatitude() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Verify latitude is Available ");
		result.setAttribute("Expected",
				"latitude should Be available for All Hotels");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			try {
				Assert.assertTrue(hotelinfo.getLatitude().length() > 0);
				result.setAttribute("Actual", "All hotels Contains a latitude");
			} catch (Exception e) {
				result.setAttribute("Actual", "latitude is not Existed of : "
						+ hotelinfo.getHotelName());
				Assert.fail();
				throw new Exception(e);
			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 15)
	public void Availabilityoflongitude() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Verify longitude is Available ");
		result.setAttribute("Expected",
				"longitude should Be available for All Hotels");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			try {
				Assert.assertTrue(hotelinfo.getLongitude().length() > 0);
				result.setAttribute("Actual", "All hotels Contains a longitude");
			} catch (Exception e) {
				result.setAttribute("Actual", "longitude is not Existed of : "
						+ hotelinfo.getHotelName());
				Assert.fail();
				throw new Exception(e);
			}

		}
	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 16)
	public void AvailabilityofCheckInDate() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Verify Hotel check in Date");
		result.setAttribute("Expected",
				"Check in Date values should be the Date which user made the search");

		/******************************************************************/

		properties = readProperty("Resources/parameters.properties");

		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getCheckinDate().equalsIgnoreCase(
					properties.get("checkInDate").replace("/", "-"))) {

				result.setAttribute("Actual", "Check In Date Is Correct");
			}

			else {

				result.setAttribute(
						"Actual",
						"Check In Date Is InCorrect Of Hotel:"
								+ hotelinfo.getHotelName());
				Assert.fail();
			}

		}
	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 17)
	public void AvailabilityofCheckOutDate() {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Verify Hotel check out Date");
		result.setAttribute("Expected",
				"Check out Date values should be the Date which user made the search");

		/******************************************************************/

		properties = readProperty("Resources/parameters.properties");
		ArrayList<Data> hotellist = response.getResp().getHotellist();

		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {
			Data hotelinfo = itr.next();

			if (hotelinfo.getCheckoutDate().equalsIgnoreCase(
					properties.get("checkOutDate").replace("/", "-"))) {

				result.setAttribute("Actual", "Check Out Date Is Correct");
			}

			else {

				result.setAttribute(
						"Actual",
						"Check Out Date Is InCorrect Of Hotel:"
								+ hotelinfo.getHotelName());
				Assert.fail();
			}

		}
	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 18)
	public void checkRoomIDAvailable() throws Exception {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availablity of the Hotel Room Id for all hotels");
		result.setAttribute("Expected",
				"All Hotel Room IDs should be available");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();
		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();
			ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
			Iterator<RoomsList> itrr = roomlist.iterator();

			while (itrr.hasNext()) {

				RoomsList roominfo = itrr.next();

				try {
					AssertJUnit.assertTrue(roominfo.getId().length() > 0);
					result.setAttribute("Actual",
							"All hotel Room Ids are existed");
				} catch (Exception e) {

					result.setAttribute(
							"Actual",
							"Room ID Not exist of Hotel : "
									+ hotelinfo.getHotelName() + ":"
									+ roominfo.getRoomName());
					Assert.fail();
					throw new Exception(e);

				}
			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 19)
	public void checkRoomTypeAvailable() throws Exception {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availablity of the Hotel roomType for all hotels");
		result.setAttribute("Expected",
				"All Hotel roomType should be available");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();
		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();
			ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
			Iterator<RoomsList> itrr = roomlist.iterator();

			while (itrr.hasNext()) {

				RoomsList roominfo = itrr.next();

				try {
					AssertJUnit.assertTrue(roominfo.getRoomType().length() > 0);
					result.setAttribute("Actual",
							"All hotel roomType are existed");
				} catch (Exception e) {

					result.setAttribute(
							"Actual",
							"room Type Not exist of Hotel : "
									+ hotelinfo.getHotelName() + ":Room Id:"
									+ roominfo.getId());
					Assert.fail();
					throw new Exception(e);

				}
			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 20)
	public void checkRateTypeAvailable() throws Exception {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availablity of the Room rateType for all hotels");
		result.setAttribute("Expected",
				"All Room rateType  should be available");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();
		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();
			ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
			Iterator<RoomsList> itrr = roomlist.iterator();

			while (itrr.hasNext()) {

				RoomsList roominfo = itrr.next();

				try {

					AssertJUnit.assertTrue(roominfo.getRateType().length() > 0);

					result.setAttribute("Actual",
							"All room rateType are existed");
				} catch (Exception e) {

					result.setAttribute(
							"Actual",
							"rate Type Not exist of Hotel : "
									+ hotelinfo.getHotelName() + ":"
									+ roominfo.getRoomName() + ":Room Id:"
									+ roominfo.getId());
					Assert.fail();
					throw new Exception(e);

				}
			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 21)
	public void checkRoomBookTypeAvailable() throws Exception {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName",
				"Check Availablity of the Room BookType for all hotels");
		result.setAttribute("Expected",
				"All Hotel Room BookType  should be available");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();
		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();
			ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
			Iterator<RoomsList> itrr = roomlist.iterator();

			while (itrr.hasNext()) {

				RoomsList roominfo = itrr.next();

				if (roominfo.getBookType().length() > 0) {

					if (roominfo.getBookType().equalsIgnoreCase("Con")
							|| roominfo.getBookType().equalsIgnoreCase("Req")) {

						result.setAttribute("Actual",
								"Book Type Available for all hotels");

					}

					else {

						result.setAttribute("Actual",
								"Hotel Level Book Type Is not Available in hotel : "
										+ hotelinfo.getHotelName() + ":"
										+ roominfo.getRoomName());
						Assert.fail();
					}

				}

			}

		}

	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 22)
	public void checkDailyRateID() throws Exception {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Availablity of Daily Rate Id");
		result.setAttribute("Expected",
				"Daily Rate Id should be existed for each day");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();
		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();
			ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
			Iterator<RoomsList> itrr = roomlist.iterator();

			while (itrr.hasNext()) {

				RoomsList roominfo = itrr.next();

				ArrayList<DailyRateList> dailyratelist = roominfo
						.getDailyRateList();
				Iterator<DailyRateList> itrrr = dailyratelist.iterator();

				while (itrrr.hasNext()) {

					DailyRateList dailyrateinfo = itrrr.next();

					if (dailyrateinfo.getId().length() > 0) {
						result.setAttribute("Actual",
								"Room- Daily RateList ID Available for All hotels -rooms");

					}

					else {

						result.setAttribute("Actual",
								"Room- Daily RateList ID Available for All hotels -rooms : "
										+ hotelinfo.getHotelName());
						Assert.fail();
					}

				}

			}

		}
	}

	@Test(dependsOnMethods = { "checkHotelResultsAvailability" }, priority = 23)
	public void checkDailyRateIsAvailable() throws Exception {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Availablity of Daily Rate ");
		result.setAttribute("Expected",
				"Daily Rate should be existed for each day");

		/******************************************************************/

		ArrayList<Data> hotellist = response.getResp().getHotellist();
		Iterator<Data> itr = hotellist.iterator();

		while (itr.hasNext()) {

			Data hotelinfo = itr.next();
			ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
			Iterator<RoomsList> itrr = roomlist.iterator();

			while (itrr.hasNext()) {

				RoomsList roominfo = itrr.next();

				ArrayList<DailyRateList> dailyratelist = roominfo
						.getDailyRateList();
				Iterator<DailyRateList> itrrr = dailyratelist.iterator();

				while (itrrr.hasNext()) {

					DailyRateList dailyrateinfo = itrrr.next();

					if (dailyrateinfo.getTotalRate().length() > 0) {
						result.setAttribute("Actual",
								"Room- Daily Rate Available for All hotels -rooms");

					}

					else {

						result.setAttribute("Actual",
								"Room- Daily Rate Available for All hotels -rooms : "
										+ hotelinfo.getHotelName());
						Assert.fail();
					}

				}

			}

		}
	}

		
	}
	
	


	
	
	
	
	
	



