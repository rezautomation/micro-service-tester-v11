package com.rezgateway.automation.HotelServiceLevelTester;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.hotel.BaseHotelRunner;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.Data;
import com.rezgateway.automation.pojo.HotelSearchDetails;
import com.rezgateway.automation.pojo.RoomsList;

public class InternalHotelLogicalTest  extends RunnerBase {
  

		AvailabilityResponse response = null;
		HotelSearchDetails details = new HotelSearchDetails();
		HashMap<String, String> properties = null;
		HashMap<String, String> prop = null;
		BaseHotelRunner runner = new BaseHotelRunner();
		
		
		
		@Test( priority = 24)
		public void CheckTestDataHotelisAvailableOntheResults() {
			
			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName", "Is Specific(Internal) Hotel Available");
			result.setAttribute("Expected", "Shangrila_"+System.getProperty("city"));

			/******************************************************************/

			details.setCheckInDate("01/10/2019");
			details.setCheckOutDate("01/12/2019");
			details.setNumberOfNights("2");
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			
			response = runner.doHotelSearch(details);

			if (response.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = response.getResp().getHotellist().size();

				if (hotelcount > 0) {
	               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	               
	               
	               ArrayList<Data> hotellist = response.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city"))){
	       				isHotelFound = true;
	       				
	       			 result.setAttribute("Actual", "Shangrila_"+System.getProperty("city") +" Internal Hotel is Available");
	       			
	       			}
	       				
	       		}
	       		
	       		if(!isHotelFound){
	       		result.setAttribute("Actual","Expected Shangrila_"+System.getProperty("city") +" internal Hotel is not Available");
	       		Assert.fail();
	       		}
	       		
	               
	            } else {
	               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
				   Assert.fail();
		

				}
			}else {
				  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
				  Assert.fail();
			}
			
		}		
		
		
	
		@Test(dependsOnMethods = { "CheckTestDataHotelisAvailableOntheResults" },priority = 25)
		public void checkHotelSpecificProfitmarkUp() throws Exception {

			
			

			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName", "Is Hotel Specific(Internal) Profit MarkUp Applied(PM-15%)");
			result.setAttribute("Expected", "Rate should be 460");

			/******************************************************************/

			details.setCheckInDate("01/10/2019");
			details.setCheckOutDate("01/12/2019");
			details.setNumberOfNights("2");
			
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			response = runner.doHotelSearch(details);

			if (response.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = response.getResp().getHotellist().size();

				if (hotelcount > 0) {
	               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	               
	               
	               ArrayList<Data> hotellist = response.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city"))){
	       				isHotelFound = true;
	       				ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
	       				Iterator<RoomsList> itrr = roomlist.iterator();
	       				
	       				boolean isRoomFound = false;
	       				while (itrr.hasNext()) {

	       					RoomsList roominfo = itrr.next();
	       					
	       					if(roominfo.getRoomType().equalsIgnoreCase("Deluxe") && roominfo.getBedType().equalsIgnoreCase("Single")){
	       						isRoomFound =true;
	       						
	       						
	       						
	       						if(roominfo.getRate().equalsIgnoreCase("460")){
	       							
	       							result.setAttribute("Actual","Rate  is  460");
	       						}
	       						
	       						else{
	       							
	       							result.setAttribute("Actual","Rate is incorrect of the:" +roominfo.getRoomType()+" "+roominfo.getBedType());
	       							Assert.fail();
	       						}
	       						
	       						
	       					}
	       					
	       					
	       					
	       				}
	       				if(!isRoomFound){
	       				result.setAttribute("Actual","Expected internal Hotel Room-Bedtype is not Available");
	    	       		Assert.fail();
	    	       		
	       				}
	       			}
	       			
	       			
	       			
	       			
	       		}
	       		
	       		if(!isHotelFound){
	       		result.setAttribute("Actual","Expected Shangrila_"+System.getProperty("city") +" internal Hotel is not Available");
	       		Assert.fail();
	       		}
	       		
	               
	            } else {
	               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
				   Assert.fail();
		

				}
			}else {
				  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
				  Assert.fail();
			}
			
		
		
		
		}
		
		
		@Test(dependsOnMethods = { "CheckTestDataHotelisAvailableOntheResults" },priority = 26)
		public void MinNightsBookedRestriction() {
			
			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName", "Minimum night Restriction Check");
			result.setAttribute("Expected", "Hotel Only Should Be Available When User Search For atleast 3 nights for feb");

			/******************************************************************/
			
			details.setCheckInDate("02/08/2019");
			details.setCheckOutDate("02/12/2019");
			details.setNumberOfNights("4");
			
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			response = runner.doHotelSearch(details);
			
			
			
			if (response.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = response.getResp().getHotellist().size();

				if (hotelcount > 0) {
	               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	               
	               
	               ArrayList<Data> hotellist = response.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city")) && hotelinfo.getAvailableToBook().equalsIgnoreCase("true")){
	       				
	       				isHotelFound = true;
	       				result.setAttribute(("Actual"), "Shangrila_"+System.getProperty("city")+ " Is available on Results");
	       				
	       				details.setCheckInDate("02/07/2019");
	       				details.setCheckOutDate("02/09/2019");
	       				details.setNumberOfNights("2");
	       				
	       				response = runner.doHotelSearch(details);
	       				
	       				
	       				if (response.getStatus().equalsIgnoreCase("200")) {
	       					int hotelcoun = response.getResp().getHotellist().size();

	       					if (hotelcoun > 0) {
	       		               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	       		               ArrayList<Data> hotellis = response.getResp().getHotellist();
	       		       		Iterator<Data> it = hotellis.iterator();
	       		       		boolean isHotelFoun = false;
	       		       		while (it.hasNext()) {
	       		       			
	       		       			Data hotelinf = it.next();
	       		       			
	       		       			if(hotelinf.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city")) && hotelinf.getAvailableToBook().equalsIgnoreCase("true")){
	       		       				
	       		       				isHotelFoun = true;
	       		       				result.setAttribute(("Actual"), "Shangrila_"+System.getProperty("city")+ " Is available on Results without considering min night Restriction (search is done for 2 nights)");
	       		       				
	       		       				Assert.fail();//if hotel available for two nights search test should be failed
	       		       				
	       		       			}	
	       		       		}
	       		       		
	       		       		if(!isHotelFoun){
	       		       		result.setAttribute("Actual","Shangrila_"+System.getProperty("city") +" internal Hotel is Not Available to Book");
	       		       		
	       		       		}
	       		       		
	       		               
	       		            } else {
	       		               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
	       					   Assert.fail();
	       			

	       					}
	       				}else {
	       					  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
	       					  Assert.fail();
	       				}
	       				
	       			}	
	       		}
	       		
	       		if(!isHotelFound){
	       		result.setAttribute("Actual","Shangrila_"+System.getProperty("city") +" internal Hotel is Not Available to Book");
	       		Assert.fail();
	       		}
	       		
	               
	            } else {
	               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
				   Assert.fail();
		

				}
			}else {
				  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
				  Assert.fail();
			}
			
		}
		
		
		@Test(dependsOnMethods = { "CheckTestDataHotelisAvailableOntheResults" },priority = 27)
		public void MaxNightsBookedRestriction() {
			
			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName", "Maximum night Restriction Check");
			result.setAttribute("Expected", "Shangrila_"+System.getProperty("city")+" Hotel should Not Be Available When user search for 6 nights");

			/******************************************************************/
			
			details.setCheckInDate("02/10/2019");
			details.setCheckOutDate("02/16/2019");
			details.setNumberOfNights("6");
			
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			response = runner.doHotelSearch(details);	
			
			
			if (response.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = response.getResp().getHotellist().size();

				if (hotelcount > 0) {
	               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	               
	               
	               ArrayList<Data> hotellist = response.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city")) && hotelinfo.getAvailableToBook().equalsIgnoreCase("true")){
	       				
	       				isHotelFound=true;
	       				result.setAttribute("Actual","Shangrila_"+System.getProperty("city")+" Hotel Is Coming To The Results Without Considering Max Night Restriction");
	       				Assert.fail();
	       				
	       			}	
	       		}
	       		
	       		if(!isHotelFound){
	       			
	       		result.setAttribute("Actual","Shangrila_"+System.getProperty("city") +" internal Hotel is Not coming to the results based on max night Restriction");
	       		
	       		}
	       		
	               
	            } else {
	               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
				   Assert.fail();
		

				}
			}else {
				  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
				  Assert.fail();
			}
			
		}	
		
		
		@Test(dependsOnMethods = { "CheckTestDataHotelisAvailableOntheResults" },priority = 28)
		public void CheckStopSalesBlackout() {
			
			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName", "Stop Sales Blackout Check");
			result.setAttribute("Expected", "Shangrila_"+System.getProperty("city")+" Hotel should Not Be Available When user search ");

			/******************************************************************/
		
		
			details.setCheckInDate("03/09/2019");
			details.setCheckOutDate("03/13/2019");
			details.setNumberOfNights("4");
			
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			response = runner.doHotelSearch(details);	
			
			if (response.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = response.getResp().getHotellist().size();

				if (hotelcount > 0) {
	               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	               
	               
	               ArrayList<Data> hotellist = response.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city")) ){
	       				
	       				isHotelFound=true;
	       				
	       				if(hotelinfo.getAvailableToBook().equalsIgnoreCase("true")){
	       					
	       					result.setAttribute("Actual","Shangrila_"+System.getProperty("city")+" Hotel Is Coming To The Results Without Considering Stop Sales Blackout Restriction");
		       				Assert.fail();
		       				break;
	       				}
	       				
	       				else {
	       					
	       					result.setAttribute("Actual","Shangrila_"+System.getProperty("city")+" Hotel Is not Coming To The Results Considering Stop Sales Blackout Restriction");
		       				
		       				break;
							
						}
	       				
	       				
	       			}
	       			
	       		
	       		}
	       		
	       		if(!isHotelFound){
	       			
	       		result.setAttribute("Actual","Shangrila_"+System.getProperty("city") +" internal Hotel is Not coming to the results");
	       			Assert.fail();
	       			
	       		
	       		}
	       		
	               
	            } else {
	               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
				   Assert.fail();
		

				}
			}else {
				  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
				  Assert.fail();
			}
			
			
			

		}
		
		@Test(dependsOnMethods = { "CheckTestDataHotelisAvailableOntheResults" },priority = 29)
		public void onrequestcontractcheck_hotellevel() {
			
			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName", "On Request Contract Check-hotel level");
			result.setAttribute("Expected", "Shangrila_"+System.getProperty("city")+" Hotel should Come to the results as an onrequest contracted hotel ");

			/******************************************************************/
			
			details.setCheckInDate("05/09/2019");
			details.setCheckOutDate("05/13/2019");
			details.setNumberOfNights("4");
			
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			response = runner.doHotelSearch(details);
			
			if (response.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = response.getResp().getHotellist().size();

				if (hotelcount > 0) {
	               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	               
	               
	               ArrayList<Data> hotellist = response.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city"))){
	       				
	       				isHotelFound=true;
	       				if(hotelinfo.getBookType().equalsIgnoreCase("REQ")){
	       					
	       					result.setAttribute("Actual","Shangrila_"+System.getProperty("city")+" Hotel Is Coming To The Results as an On-Requested Hotel");
	       					break;
	       				}
	       				else{
	       					result.setAttribute("Actual","Shangrila_"+System.getProperty("city")+" Hotel Is Not Coming To The Results as an On-Requested Hotel");
	       					Assert.fail();
	       					break;
	       				}
	       				
	       				
	       			
	       				
	       			}	
	       		}
	       		
	       		if(!isHotelFound){
	       			
	       		result.setAttribute("Actual","Shangrila_"+System.getProperty("city") +" internal Hotel is Not coming to the results");
	       		Assert.fail();
	       		}
	       		
	               
	            } else {
	               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
				   Assert.fail();
		

				}
			}else {
				  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
				  Assert.fail();
			}
			
		}
		

		
		@Test(dependsOnMethods = { "CheckTestDataHotelisAvailableOntheResults" },priority = 30)
		public void discountpromocheck(){
			
			
			/******************************************************************/
			ITestResult result = Reporter.getCurrentTestResult();
			result.setAttribute("TestName", "Discounted Hotel Check");
			result.setAttribute("Expected", "Shangrila_"+System.getProperty("city")+"discounted values of the deluxe single room rate should be 120 USD");

			/******************************************************************/
	
	
			details.setCheckInDate("06/09/2019");
			details.setCheckOutDate("06/10/2019");
			details.setNumberOfNights("1");
			
			details.setTransectionID(UUID.randomUUID().toString().replace("-", ""));
			
			response = runner.doHotelSearch(details);
			
			
			if (response.getStatus().equalsIgnoreCase("200")) {
				int hotelcount = response.getResp().getHotellist().size();

				if (hotelcount > 0) {
	               result.setAttribute("Actual", response.getResp().getStatus()+ " Hotel Count: " + hotelcount);
	               
	               
	               ArrayList<Data> hotellist = response.getResp().getHotellist();
	       		Iterator<Data> itr = hotellist.iterator();
	       		boolean isHotelFound = false;
	       		while (itr.hasNext()) {
	       			
	       			Data hotelinfo = itr.next();
	       			
	       			if(hotelinfo.getHotelName().equalsIgnoreCase("Shangrila_"+System.getProperty("city"))){
	       				isHotelFound = true;
	       				ArrayList<RoomsList> roomlist = hotelinfo.getRoomlist();
	       				Iterator<RoomsList> itrr = roomlist.iterator();
	       				
	       				boolean isRoomFound = false;
	       				while (itrr.hasNext()) {

	       					RoomsList roominfo = itrr.next();
	       					
	       					if(roominfo.getRoomType().equalsIgnoreCase("Deluxe") && roominfo.getBedType().equalsIgnoreCase("Single")){
	       						isRoomFound =true;
	       						
	       						
	       						
	       						if(roominfo.getRate().equalsIgnoreCase("120")){
	       							System.out.println("xxxxx");
	       							
	       							result.setAttribute("Actual","Rate  is  120");
	       						}
	       						
	       						else{
	       							
	       							result.setAttribute("Actual","Rate is incorrect of the:" +roominfo.getRoomType()+" "+roominfo.getBedType());
	       							Assert.fail();
	       						}
	       						
	       						
	       					}
	       					
	       					
	       					
	       				}
	       				if(!isRoomFound){
	       				result.setAttribute("Actual","Expected internal Hotel Room-Bedtype is not Available");
	    	       		Assert.fail();
	    	       		
	       				}
	       			}
	       			
	       			
	       			
	       			
	       		}
	       		
	       		if(!isHotelFound){
	       		result.setAttribute("Actual","Expected Shangrila_"+System.getProperty("city") +" internal Hotel is not Available");
	       		Assert.fail();
	       		}
	       		
	               
	            } else {
	               result.setAttribute("Actual","Hotel Results(Internal and thirdparty) are not Available");
				   Assert.fail();
		

				}
			}else {
				  result.setAttribute("Actual","Error Response ===> Response Code :"+response.getStatus());
				  Assert.fail();
			}
			
	
	
}

		
		
}
